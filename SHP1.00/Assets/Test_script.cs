﻿using UnityEngine;
using Fungus;
using System.Collections;

public class Test_script : MonoBehaviour {
    public Flowchart flowchart1;
    public Flowchart flowchart2;
    public Flowchart flowchart3;
    public Flowchart flowchart4;
    public Flowchart flowchart5;
    public Flowchart flowchart6;
    public Vector2 pos1 = new Vector2();
    public Vector2 size = new Vector2();
    public GUIStyle labelstyle;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {


    }
    void OnGUI()
    {
        if (flowchart3.GetIntegerVariable("chart") == 1)
        {
            GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Label(new Rect(10, 10, 100, 20), flowchart1.SelectedBlock.BlockName, labelstyle);
            GUI.EndGroup();
        }
        if(flowchart3.GetIntegerVariable("chart") == 2)
        {
            GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Label(new Rect(10, 10, 100, 20), flowchart2.SelectedBlock.BlockName, labelstyle);
            GUI.EndGroup();
        }
        if (flowchart3.GetIntegerVariable("chart") == 3)
        {
            GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Label(new Rect(10, 10, 100, 20), flowchart3.SelectedBlock.BlockName, labelstyle);
            GUI.EndGroup();
        }
        if (flowchart3.GetIntegerVariable("chart") == 4)
        {
            GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Label(new Rect(10, 10, 100, 20), flowchart4.SelectedBlock.BlockName, labelstyle);
            GUI.EndGroup();
        }
        if (flowchart3.GetIntegerVariable("chart") == 5)
        {
            GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Label(new Rect(10, 10, 100, 20), flowchart5.SelectedBlock.BlockName, labelstyle);
            GUI.EndGroup();
        }
        if (flowchart3.GetIntegerVariable("chart") == 6)
        {
            GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Label(new Rect(10, 10, 100, 20), flowchart6.SelectedBlock.BlockName, labelstyle);
            GUI.EndGroup();
        }
    }
}
