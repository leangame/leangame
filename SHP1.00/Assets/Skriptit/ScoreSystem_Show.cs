﻿using UnityEngine;
using Fungus;
using System.Collections;

public class ScoreSystem_Show : MonoBehaviour {
    public Flowchart flowchart;
    public float maxPoints = 200;
    public int maxMinusPoints = 50;
    public float lpy = 0;
    public float asiakast = 0;
    public float tyyt = 0;
    public float potilaat = 0;
    public Vector2 pos1 = new Vector2(20, 240);
    public Vector2 pos2 = new Vector2(20, 280);
    public Vector2 pos3 = new Vector2(20, 320);
    public Vector2 pos4 = new Vector2(20, 360);
    public Vector2 size = new Vector2(120, 39);
    float t = 0;
    public Texture2D emptyTex;
    public Texture2D fullTex;
    public GUIStyle btnStyleFull;
    public GUIStyle btnStyleEmpty;

    // Use this for initialization
    void Start () {
	
	}

    void OnGUI()
    {
        GUI.skin.button.stretchHeight = true;
        GUI.skin.button.stretchWidth = true;
        //btnStyle = GUI.skin.button.stretchHeight
        //GUI.skin.customStyles[0].border = new RectOffset(0, 0, 0, 0);
        //GUI.skin.button.fixedHeight = 0;
        //GUI.skin.button.fixedWidth = 0;
        //draw the background:
        GUI.BeginGroup(new Rect(pos1.x, pos1.y, size.x, size.y));
            GUI.Box(new Rect(0, 0, size.x, size.y), "Läpimenoaika", btnStyleEmpty);

        //draw the filled-in part:
            GUI.BeginGroup(new Rect(0, 0, size.x * lpy, size.y));
                GUI.Box(new Rect(0, 0, size.x, size.y), "Läpimenoaika", btnStyleFull);
            GUI.EndGroup();
        GUI.EndGroup();

        //draw the background:
        GUI.BeginGroup(new Rect(pos2.x, pos2.y, size.x, size.y));
            GUI.Box(new Rect(0, 0, size.x, size.y), "Asiakastyytyväisyys", btnStyleEmpty);

        //draw the filled-in part:
            GUI.BeginGroup(new Rect(0, 0, size.x * asiakast, size.y));
                GUI.Box(new Rect(0, 0, size.x, size.y), "Asiakastyytyväisyys", btnStyleFull);
            GUI.EndGroup();
        GUI.EndGroup();

        //draw the background:
        GUI.BeginGroup(new Rect(pos3.x, pos3.y, size.x, size.y));
            GUI.Box(new Rect(0, 0, size.x, size.y), "Työtyytyväisyys", btnStyleEmpty);

        //draw the filled-in part:
            GUI.BeginGroup(new Rect(0, 0, size.x * tyyt, size.y));
                GUI.Box(new Rect(0, 0, size.x, size.y), "Työtyytyväisyys", btnStyleFull);
            GUI.EndGroup();
        GUI.EndGroup();

        //draw the background:
        GUI.BeginGroup(new Rect(pos4.x, pos4.y, size.x, size.y));
            GUI.Box(new Rect(0, 0, size.x, size.y), "Hoidetut potilaat",btnStyleEmpty);

        //draw the filled-in part:
            GUI.BeginGroup(new Rect(0, 0, size.x * potilaat, size.y));
                GUI.Box(new Rect(0, 0, size.x, size.y), "Hoidetut potilaat", btnStyleFull);
            GUI.EndGroup();
        GUI.EndGroup();
    }

    // Update is called once per frame
    void Update () {
        //barDisplay = Time.time * 0.05f;
        if (flowchart.GetBooleanVariable("Score") == true && t < 1)
        {
            lpy = Mathf.Lerp(0, (flowchart.GetIntegerVariable("lpy")+maxMinusPoints)/maxPoints, t);
            asiakast = Mathf.Lerp(0, (flowchart.GetIntegerVariable("asiakast")+maxMinusPoints)/maxPoints, t);
            tyyt = Mathf.Lerp(0, (flowchart.GetIntegerVariable("tyyt")+maxMinusPoints)/maxPoints, t);
            potilaat = Mathf.Lerp(0, (flowchart.GetIntegerVariable("potilaat")+maxMinusPoints)/maxPoints, t);
            t += 0.3f * Time.deltaTime;
        }
    }
}
