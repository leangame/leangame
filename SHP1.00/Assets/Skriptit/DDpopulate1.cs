﻿using UnityEngine;
using UnityEngine.UI;
using Fungus;
using System.Collections.Generic;

public class DDpopulate1 : MonoBehaviour
{

    public Flowchart Testipalaute;
    public Flowchart muuttujat;
    public Dropdown dropdownvarsi1;
    public string piiri;
    
    List<string> alueetvarsi = new List<string>()
    
    {
        "Valitse tulosalueesi", "[P1] Asiantuntijapalvelut", "[P2] TOTEK", "[T1] TULES", "[T2] Sydänkeskus",
        "[T3] Vatsaelinkirurgian ja urologian klinikka", "[T4] Neuro", "[T5] Medisiininen", "[T6] Operatiivinen toiminta ja syöpätaudit",
        "[T7] Naistenklinikka", "[T8] Lasten ja nuorten klinikka"
    };

    List<string> alueetsata = new List<string>()
    {
        "Valitse tulosalueesi", "YHTYMÄHALLINTO / Keskustoimisto", "YHTYMÄHALLINTO / Henkilöstö- ja laskentapalvelut", "SAIRAANHOIDON TOIMIALUE / Konservatiivinen hoito",
        "SAIRAANHOIDON TOIMIALUE / Lasten- ja naistentaudit sekä synnytykset", "SAIRAANHOIDON TOIMIALUE / Operatiivinen hoito",
        "SAIRAANHOIDON TOIMIALUE / Ensihoito- ja päivystys", "SAIRAANHOIDON TOIMIALUE / Psykiatrinen hoito", "SOSIAALIPALVELUT / Sosiaalipalvelujen hallinto ja kiinteistöt",
        "SOSIAALIPALVELUT / Sosiaalipalvelujen Antinkartanon kuntoutuskeskus", "SOSIAALIPALVELUT / Sosiaalipalvelujen asumispalvelut",
        "LIIKELAITOS SataDiag / Liikelaitoksen hallinto", "LIIKELAITOS SataDiag / Lääkehuolto", "LIIKELAITOS SataDiag / Kliininen fysiologia ja isotooppilääketiede",
        "LIIKELAITOS SataDiag / Kliininen kemia", "LIIKELAITOS SataDiag / Kliininen mikrobiologia", "LIIKELAITOS SataDiag / Kliininen neurofysiologia", "LIIKELAITOS SataDiag / Kuvantamistoiminta",
        "LIIKELAITOS SataDiag / Patologia", "LIIKELAITOS SataDiag / Infektioyksikkö", "HUOLTOKESKUS / Huoltokeskuksen hallinto",
        "HUOLTOKESKUS / Hankintapalvelut ja monistuskeskus", "HUOLTOKESKUS / Logistiikkakeskuksen taseyksikkö", "HUOLTOKESKUS / Tekninen keskus",
        "HUOLTOKESKUS / Ruokapalvelut", "HUOLTOKESKUS / Siivouskeskus"
    };

    List<string> alueetvaasa = new List<string>()
    {
        "Valitse tulosalueesi", "Palvelualue", "Lääkäreiden ja asiantuntijoiden palvelualue", "Akuuttihoidon palvelualue", "Avohoidon palvelualue",
        "Vuodeosastojen palvelualue", "Naisten ja lasten palvelualue", "Psykiatrian palvelualue", "Hoidon palveluyksikkö", "Sairaanhoidon tuen palvelualue",
        "Diagnostiikkakeskus", "Huollon palvelualue", "Hallinnon palvelualue"
    };

    List<string> virhe = new List<string>()
    {
        "Jotain meni vikaan"
    };
    public string tulos;
    public int tulo;


	// Use this for initialization
	void Start ()
    {
        Flowchart Testipalaute = FindObjectOfType<Flowchart>();
        Flowchart muuttujat = FindObjectOfType<Flowchart>();
        Block activatecanvas = Testipalaute.FindBlock("activatecanvas");
        if (Testipalaute.IsActive())
        {
            PopulateList();
        }
	}
	
	void PopulateList()
    {
            piiri = muuttujat.GetStringVariable("piiri");

            if (piiri == "varsinais") { dropdownvarsi1.AddOptions(alueetvarsi); }

            else if (piiri == "satakunta") { dropdownvarsi1.AddOptions(alueetsata); }

            else if (piiri == "vaasa") { dropdownvarsi1.AddOptions(alueetvaasa); }

            else { dropdownvarsi1.AddOptions(virhe); }
	}

    public void Dropdown_IndexChanged(int index)
    {
        if (piiri == "varsinais") { tulos = alueetvarsi[index]; Testipalaute.SendFungusMessage("varsi"); }

        else if (piiri == "satakunta") {tulos = alueetsata[index]; Testipalaute.SendFungusMessage("sata"); }

        else if (piiri == "vaasa") {tulos = alueetvaasa[index]; Testipalaute.SendFungusMessage("vaasa"); }

        tulo = index;
        Testipalaute.SetStringVariable("tulos", tulos);
        Testipalaute.SetIntegerVariable("tulo", tulo);
        
        Debug.Log(tulos);
    }

}
