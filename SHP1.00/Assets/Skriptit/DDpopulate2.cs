﻿using UnityEngine;
using UnityEngine.UI;
using Fungus;
using System.Collections.Generic;

public class DDpopulate2 : MonoBehaviour
{

    public Flowchart Testipalaute;
    public Flowchart muuttujat;
    public Dropdown dropdownvarsi2;
    public Dropdown dropdownvarsi1;
    int tulo;

    List<string> P1 = new List<string>()
    {
        "valitse osasto", "Alueellinen apuvälinekeskus", "Fysiatrian yksikkö Paimio", "Fysioterapia", "Hallinto", "Kuntoutusohjaus",
        "Lääketieteellinen fysiikka", "Psykologipalvelut", "Puheterapia", "Ravitsemusterapia", "Sosiaalityö",
        "Toimintaterapia"
    };

    List<string> P2 = new List<string>()
    {
        "valitse osasto", "[ATOTEK] ATOTEK", "[HTUKI] Hengitystukiyksikkö", "[KIPU] Kipuklinikka", "[KTOTEK] KTOTEK", "[MTOTEK]",
        "[ICU] Tehohoitopalvelut", "[TTOTEK] TTOTEK", "[UTOTEK] UTOTEK"
    };

    List<string> T1 = new List<string>()
    {
        "valitse osasto", "[ARTRO] Artro", "[FYS] Fysiatria", "[KKIR] Käsikirurgia", "[RORT] Reumaortopedia", "[SELKÄ] Selkäpotilaan hoito",
        "[PROTE] Tekonivekirurgia", "[TRAUMA] Traumojen hoito", "TULES varahenkilöstö"
    };

    List<string> T2 = new List<string>()
    {
        "valitse osasto", "Sydänkeskus"
    };

    List<string> T3 = new List<string>()
    {
        "valitse osasto", "Päivystyskirurgia", "Urologia", "Vatsaelinkirurgia", "[VERIS] Verisuonikirurgisen potilaan hoito"
    };

    List<string> T4 = new List<string>()
    {
        "valitse osasto", "[AVH] Aivoverenkiertohäiriöiden hoito", "[KV] Kuntoutus ja aivovammapotilaan hoito", "[NKIR] Neurokirurgisen potilaan hoito",
        "[NEU] Yleisneurologia"
    };

    List<string> T5 = new List<string>()
    {
        "valitse osasto", "[ASIS] Akuutti sisätautihoito", "[END] Endokrinologia", "[GAS] Gastroenterologia", "[HEM] Hematologia ja kantasolusiirtoyksikkö",
        "[IHO] Ihotautien klinikka", "[INF] Infektiotaudit", "[KEU] Keuhkosairaudet", "[KIP] Kliininen fysiologia, isotooppi- ja PET- tutkimukset",
        "[KGEN] Kliininen genetiikka", "[MUN] Nefrologia ja dialyysihoidot", "Päiväsairaala", "[REU] Reumatologia",
        "[YSIS] Sisätaudit", "[TYÖ] Työlääketieteen klinikka"
    };

    List<string> T6 = new List<string>()
    {
        "valitse osasto", "[GEN] Kliininen genetiikka", "[KOR] Korvaklinikka", "[PLY] Plastiikka- ja yleiskirurgia", "[SIL] Silmäklinikka", "[SUU] Suu- ja leukasairauksien klinikka",
        "[SYÖ] Syöpäklinikka"
    };

    List<string> T7 = new List<string>()
    {
        "valitse osasto", "[GYN] Gynekologinen hoito", "[GSYO] Gynekologisen syövän hoito", "[OBST] Raskauden ja synnytyksen hoito (0)"
    };

    List<string> T8 = new List<string>()
    {
        "valitse osasto", "[LNKIR] Lasten ja nuorten kirurgia", "Lasten ja nuorten klinikan varahenkilöstö", "[PED] Lasten ja nuorten sairaanhoito",
        "[LHEM] Lasten ja nuorten veri- ja syöpäsairauksien hoito", "[LSNEU] Lasten neurologia", "[PNATO] Vastasyntyneiden sairaanhoito"
    };

    List<string> virhe = new List<string>()
    {
        "Jotain meni vikaan"
    };
    public string osasto;


    // Use this for initialization
    void Start()
    {
        Flowchart Testipalaute = FindObjectOfType<Flowchart>();
        Flowchart muuttujat = FindObjectOfType<Flowchart>();
        Block activatecanvas = Testipalaute.FindBlock("activatecanvas");
        PopulateList2();

    }

    void PopulateList2()
    {
        tulo = Testipalaute.GetIntegerVariable("tulo");

        if (tulo == 1) { dropdownvarsi2.AddOptions(P1); }
        else if (tulo == 2) { dropdownvarsi2.AddOptions(P2); }
        else if (tulo == 3) { dropdownvarsi2.AddOptions(T1); }
        else if (tulo == 4) { dropdownvarsi2.AddOptions(T2); }
        else if (tulo == 5) { dropdownvarsi2.AddOptions(T3); }
        else if (tulo == 6) { dropdownvarsi2.AddOptions(T4); }
        else if (tulo == 7) { dropdownvarsi2.AddOptions(T5); }
        else if (tulo == 8) { dropdownvarsi2.AddOptions(T6); }
        else if (tulo == 9) { dropdownvarsi2.AddOptions(T7); }
        else if (tulo == 10) { dropdownvarsi2.AddOptions(T8); }
        else { dropdownvarsi2.AddOptions(virhe); }
    }

    public void Dropdown_IndexChanged(int index)
    {
        GameObject dropdownvarsi1 = GameObject.Find("dropdownvarsi1");
        DDpopulate1 ddPopulate1 = dropdownvarsi1.GetComponent<DDpopulate1>();
        int valinta = ddPopulate1.tulo;
        if (valinta == 1) { osasto = P1[index]; }
            else if (valinta == 2) { osasto = P2[index]; }
            else if (valinta == 3) { osasto = T1[index]; }
            else if (valinta == 4) { osasto = T2[index]; }
            else if (valinta == 5) { osasto = T3[index]; }
            else if (valinta == 6) { osasto = T4[index]; }
            else if (valinta == 7) { osasto = T5[index]; }
            else if (valinta == 8) { osasto = T6[index]; }
            else if (valinta == 9) { osasto = T7[index]; }
            else if (valinta == 10) { osasto = T8[index]; }
            Testipalaute.SetStringVariable("osasto", osasto);

            Debug.Log(osasto);
            Testipalaute.SendFungusMessage("osasto");
    }

}
