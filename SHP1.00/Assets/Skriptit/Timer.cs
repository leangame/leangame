﻿using UnityEngine;
using System.Collections;
using Fungus;

[EventHandlerInfo("Timers",
					"Simple Timer",
					"Executes the block after an amount of time has elapsed")]
public class Timer : EventHandler 
{
	public float duration;

	void Start(){
		Invoke ("TimerExpired", duration);
	}

	void TimerExpired()
	{
		ExecuteBlock();
	}
}
