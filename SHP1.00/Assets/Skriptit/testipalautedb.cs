﻿using UnityEngine;
using System.Collections;
using Fungus;

public class testipalautedb : MonoBehaviour
{
    public Flowchart testipalaute;

    public string myString = "";

    public float timeToWait = 5f;

    public int i = 0;

    private string testreviewURL = "http://localhost/dbtutorial/testi_palaute.php?";

    // Use this for initialization
    void Start()
    {
        //Haetaan oikea Flowchart ja blocki Funguksesta
        Flowchart testipalaute = GameObject.FindObjectOfType<Flowchart>();
        Block kiitos = GameObject.FindObjectOfType<Block>();
    }



    public void OnEnter()
    {

        //kun päästään oikeaan blockiin  haetaan pisteet
        string piti = testipalaute.GetStringVariable("piti");
        string ehdotus = testipalaute.GetStringVariable("ehdotus");


        if (i < 1)
        {
            //varmistetaan, että kanta päivitetään vain kerran
            StartCoroutine(PostScores(piti, ehdotus));
            Debug.Log(piti + " " + ehdotus);
            Debug.Log("Tietokanta täytetty");
            i++;
        }
    }

    //Update is called once per frame
    void Update()
    {
        //tarkkailee ollaanko päästy haluttuun blockiin
        Block kiitos = testipalaute.FindBlock("kiitos");
        if (kiitos.IsExecuting())
        {
            OnEnter();
        }
    }
    IEnumerator PostScores(string piti, string ehdotus)
    {   //Yhdistetään haluttuun .php scriptiin ja lähetetään pisteet mysql stringinä

        string post_url = testreviewURL + "&piti=" + piti + "&ehdotus=" + ehdotus;

        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Odotetaan kunnes vahvistus on perillä

        if (hs_post.error != null)
        {
            Debug.Log("There was an error posting the reviews: " + hs_post.error);
        }
    }
}