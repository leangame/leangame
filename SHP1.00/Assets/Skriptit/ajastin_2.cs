﻿using UnityEngine;
using System.Collections;
using Fungus;

public class ajastin_2 : MonoBehaviour {
	public Flowchart varastopeli;
	public float t_ajastin;
	int aika;
	// Use this for initialization
	void Start () 
	{
		Flowchart varastopeli = GameObject.FindObjectOfType<Flowchart>();
		Block ajastin_2 = GameObject.FindObjectOfType<Block>();
	}
	
	// Update is called once per frame

	private void Update()
	{
	Block ajastin_2 = varastopeli.FindBlock("ajastin_2");
	if(ajastin_2.IsExecuting())
		{
		t_ajastin += Time.deltaTime;
		aika = Mathf.RoundToInt(t_ajastin);
		GameObject.FindObjectOfType<Flowchart>().SetFloatVariable("t_ajastin", t_ajastin);
		GameObject.FindObjectOfType<Flowchart>().SetIntegerVariable("aika", aika);
		}
	}

}