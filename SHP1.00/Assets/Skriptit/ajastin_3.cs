﻿using UnityEngine;
using System.Collections;
using Fungus;

public class ajastin_3 : MonoBehaviour {
	public Flowchart varastopeli;
	public float k_ajastin;
	int aika;
	// Use this for initialization
	void Start () 
	{
		Flowchart varastopeli = GameObject.FindObjectOfType<Flowchart>();
		Block ajastin_3 = GameObject.FindObjectOfType<Block>();
	}
	
	// Update is called once per frame

	private void Update()
	{
	Block ajastin_3 = varastopeli.FindBlock("ajastin_3");
	if(ajastin_3.IsExecuting())
		{
		k_ajastin += Time.deltaTime;
		aika = Mathf.RoundToInt(k_ajastin);
		GameObject.FindObjectOfType<Flowchart>().SetFloatVariable("k_ajastin", k_ajastin);
		GameObject.FindObjectOfType<Flowchart>().SetIntegerVariable("aika", aika);
		}
	}

}