﻿using UnityEngine;
using System.Collections;
using Fungus;
using System.Collections.Generic;
using UnityEngine.UI;

public class DDpopulate3 : MonoBehaviour
{

    public Flowchart Testipalaute;
    public Flowchart muuttujat;
    public Dropdown dropdownsata2;
    public Dropdown dropdownvarsi1;
    public int valinta;

    List<string> keskustoimisto = new List<string>()
    {
        "Keskustoimisto", "Yhteistoiminta ja työhyvinvointi", "Sairaanhoidon hallinto", "Tutkimus- ja kehittämistoiminta",
        "Potilaskertomusarkisto", "Asiakaspalvelukeskus"
    };

    List<string> henkilosto = new List<string>()
    {
        "Henkilöstöpalvelut", "Laskentapalvelut", "Potilaslaskutus"
    };

    List<string> konservatiivinen = new List<string>()
    {
        "Konservatiivisen hallinto", "Fysiatria ja kuntoutus", "Ihotaudit", "Keuhkosairaudet", "Neurologia", "Sisätaudit",
        "Sydänyksikkö", "Reumasairaudet", "Syödätaudit"
    };

    List<string> lasten = new List<string>()
    {
        "Lasten- ja naistentautien sekä synnytysten hallinto", "Lastentaudit", "Naistentaudit ja synnytykset"
    };

    List<string> operatiivinen = new List<string>()
    {
        "Operatiivisen hallinto", "Hammas-, suu- ja leukasairaudet", "Leikkaus ja anestesia", "Kirurgia", "Korva-, nenä- ja kurkkutaudit",
        "Silmätaudit", "Välinehuoltokeskus"
    };

    List<string> ensihoito = new List<string>()
    {
        "Ensihoidon ja päivystyksen hallinto", "Päivystystoiminta", "Ensihoitopalvelut"
    };

    List<string> psykiatrinen = new List<string>()
    {
        "Psykiatrian hallinto, kehittämis-, tutkimus-, ja palveluyksikkö", "Aikuispsykiatria", "Lastenpsykiatria", "Nuorisopsykiatria"
    };

    List<string> kuntoutuskeskus = new List<string>()
    {
        "Antinkartanon toimintakeskus", "Sos. palvelukodit", "Allasosasto ja laitoshuoltajat"
    };

    List<string> asumispalvelut = new List<string>()
    {
        "Sos. ryhmäkodit", "Sos. muut asumispalvelut", "Erityisosaamiskeskus ja perhehoito"
    };

    List<string> liikelaitos = new List<string>()
    {
        "LL hallinto ja tutkimus- ja kehittämistoiminta", "Auria Biopankki Porin yksikkö"
    };

    List<string> lääkehuolto = new List<string>()
    {
        "SKS sairaala-apteekki", "Psy lääkekeskus", "Rauman lääkekeskus", "Porin lääkekeskus + K:pää"
    };

    List<string> isotooppi = new List<string>()
    {
        "SKS kliininen fysiologia ja isotooppilääketiede", "Rauman kliininen fysiologia", "Fyysikot"
    };

    List<string> kemia = new List<string>()
    {
        "SKS kliininen kemia", "Maantiekatu laboratorio", "Ulvilan laboratorio", "Kankaanpään laboratorio", "Noormarkun laboratorio", "Huittisten laboratorio",
        "Rauman laboratorio"
    };

    List<string> kuvantamis = new List<string>()
    {
        "SKS kuvantaminen + Ulvila", "Rauman kuvantaminen", "Maantienkadun kuvantaminen", "Noormarkun kuvantaminen",
        "Euran kuvantaminen", "Säkylä-Köyliö kuvantaminen", "Huittisten kuvantaminen"
    };

    List<string> logistiikka = new List<string>()
    {
        "Logistiikkapalvelut", "Lisäarvopalvelut", "Kuljetuspalvelut"
    };

    List<string> tekninen = new List<string>()
    {
        "Teknisen keskuksen hallinto", "SKS kone- ja LVI-tekniikka", "HS ja SOS talotekniikka", "SKS sähkötekniikka",
        "SKS kiinteistöpalvelut", "HS ja SOS kiinteistöpalvelut", "Rauman tekninen keskus", "SKS lääkintälaitetekniikka"
    };

    List<string> ruoka = new List<string>()
    {
        "Porin ruokapalvelut", "Harjavallan ruokapalvelut"
    };

    List<string> siivous = new List<string>()
    {
        "Siivouskeskuksen hallinto", "SKS siivouspalvelut 1", "SKS siivouspalvelut 2", "SKS siivouspalvelut 3", "SKS siivouspalvelut 4",
        "HS siivouspalvelut"
    };

    List<string> virhe = new List<string>()
    {
        "Jotain meni vikaan"
    };
    public string osasto;


    // Use this for initialization
    void Start()
    {
        Flowchart Testipalaute = FindObjectOfType<Flowchart>();
        Flowchart muuttujat = FindObjectOfType<Flowchart>();
        Block activatecanvas = Testipalaute.FindBlock("activatecanvas");
        if (Testipalaute.IsActive())
        {
            PopulateList3();
        }
    }


    void PopulateList3()
    {
        //luo dropdownlistan aiemmin valitun mukaan. SendFungusMessage siirtää pelin eteenpäin, jos osastoa ei tarvitse valita.
        valinta = Testipalaute.GetIntegerVariable("tulo");

        if (valinta == 1) { dropdownsata2.AddOptions(keskustoimisto); }
        else if (valinta == 2) { dropdownsata2.AddOptions(henkilosto); }
        else if (valinta == 3) { dropdownsata2.AddOptions(konservatiivinen); }
        else if (valinta == 4) { dropdownsata2.AddOptions(lasten); }
        else if (valinta == 5) { dropdownsata2.AddOptions(operatiivinen); }
        else if (valinta == 6) { dropdownsata2.AddOptions(ensihoito); }
        else if (valinta == 7) { dropdownsata2.AddOptions(psykiatrinen); }
        else if (valinta == 8) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 9) { dropdownsata2.AddOptions(kuntoutuskeskus); }
        else if (valinta == 10) { dropdownsata2.AddOptions(asumispalvelut); }
        else if (valinta == 11) { dropdownsata2.AddOptions(liikelaitos); }
        else if (valinta == 12) { dropdownsata2.AddOptions(lääkehuolto); }
        else if (valinta == 13) { dropdownsata2.AddOptions(isotooppi); }
        else if (valinta == 14) { dropdownsata2.AddOptions(kemia); }
        else if (valinta == 15) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 16) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 17) { dropdownsata2.AddOptions(kuvantamis); }
        else if (valinta == 18) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 19) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 20) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 21) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 22) { dropdownsata2.AddOptions(logistiikka); }
        else if (valinta == 23) { dropdownsata2.AddOptions(tekninen); }
        else if (valinta == 24) { dropdownsata2.AddOptions(ruoka); }
        else if (valinta == 25) { dropdownsata2.AddOptions(siivous); }
        else { dropdownsata2.AddOptions(virhe); }
    }

    public void Dropdown_IndexChanged(int index)
    {
        GameObject dropdownvarsi1 = GameObject.Find("dropdownvarsi1");
        DDpopulate1 ddPopulate1 = dropdownvarsi1.GetComponent<DDpopulate1>();
        int valinta = ddPopulate1.tulo;

        if (valinta == 1) { osasto = keskustoimisto[index]; }
        else if (valinta == 2) { osasto = henkilosto[index]; }
        else if (valinta == 3) { osasto = konservatiivinen[index]; }
        else if (valinta == 4) { osasto = lasten[index]; }
        else if (valinta == 5) { osasto = operatiivinen[index]; }
        else if (valinta == 6) { osasto = ensihoito[index]; }
        else if (valinta == 7) { osasto = psykiatrinen[index]; }
        else if (valinta == 8) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 9) { osasto = kuntoutuskeskus[index]; }
        else if (valinta == 10) { osasto = asumispalvelut[index]; }
        else if (valinta == 11) { osasto = liikelaitos[index]; }
        else if (valinta == 12) { osasto = lääkehuolto[index]; }
        else if (valinta == 13) { osasto = isotooppi[index]; }
        else if (valinta == 14) { osasto = kemia[index]; }
        else if (valinta == 15) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 16) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 17) { osasto = kuvantamis[index]; }
        else if (valinta == 18) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 19) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 20) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 21) { Testipalaute.SendFungusMessage("osasto"); }
        else if (valinta == 22) { osasto = logistiikka[index]; }
        else if (valinta == 23) { osasto = tekninen[index]; }
        else if (valinta == 24) { osasto = ruoka[index]; }
        else if (valinta == 25) { osasto = siivous[index]; }
        Testipalaute.SetStringVariable("osasto", osasto);
        Debug.Log(osasto);
        Testipalaute.SendFungusMessage("osasto");
    }

}