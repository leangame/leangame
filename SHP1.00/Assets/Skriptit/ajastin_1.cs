﻿using UnityEngine;
using System.Collections;
using Fungus;

public class ajastin_1 : MonoBehaviour {
	public Flowchart varastopeli;
	public float ens_ajastin;
	int aika;
	// Use this for initialization
	void Start () 
	{
		Flowchart varastopeli = GameObject.FindObjectOfType<Flowchart>();
		Block ajastin_1 = GameObject.FindObjectOfType<Block>();
	}
	
	// Update is called once per frame

	private void Update()
	{
	Block ajastin_1 = varastopeli.FindBlock("ajastin_1");

	if(ajastin_1.IsExecuting())
	{
		ens_ajastin += Time.deltaTime;
		aika = Mathf.RoundToInt(ens_ajastin);
		GameObject.FindObjectOfType<Flowchart>().SetFloatVariable("ens_ajastin", ens_ajastin);
		GameObject.FindObjectOfType<Flowchart>().SetIntegerVariable("aika", aika);
	}

}}
