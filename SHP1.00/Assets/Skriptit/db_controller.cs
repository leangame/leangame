﻿using UnityEngine;
using System;
using System.Collections;
using Fungus;

public class db_controller: MonoBehaviour 
{	public Flowchart Lataa;

    public Flowchart Testipalaute;

	public string myString = "";

    public float timeToWait = 5f;

	public int i = 0;

	private string addScoreURL = "PHP/Insert_User.php?";

		// Use this for initialization
	void Start () 
	{
		//Haetaan oikea Flowchart ja blocki Funguksesta
		Flowchart Lataa = GameObject.FindObjectOfType<Flowchart>();
        Flowchart Testipalaute = GameObject.FindObjectOfType<Flowchart>();
		Block tietokantaan = GameObject.FindObjectOfType<Block>();
	}


			
	public void OnEnter()
        {

		//kun päästään oikeaan blockiin  haetaan pisteet
		int lpy = Lataa.GetIntegerVariable("lpy");
		int asiakast = Lataa.GetIntegerVariable("asiakast");
		int tyyt = Lataa.GetIntegerVariable("tyyt");
		int potilaat = Lataa.GetIntegerVariable("potilaat");
		int virheet = Lataa.GetIntegerVariable("virheet");
        string ehdotus = Testipalaute.GetStringVariable("ehdotus");
        string tulos = Testipalaute.GetStringVariable("tulos");
        string osasto = Testipalaute.GetStringVariable("osasto");
        string kokemus = Testipalaute.GetStringVariable("kokemus");
        string auttoiko = Testipalaute.GetStringVariable("auttoiko");
		
			if(i < 1)
				{
				//varmistetaan, että kanta päivitetään vain kerran
				StartCoroutine(PostScores(lpy, asiakast, tyyt, potilaat, virheet, tulos, osasto, kokemus, auttoiko, ehdotus));
				Debug.Log(lpy + " " + asiakast + " " + tyyt + " " + potilaat + " " + virheet + " ja palaute: " + ehdotus + "/ tulosalue: " + tulos + "/ osasto: " + osasto + "/ kokeeko ymmärtävänsä Leania: " + kokemus + "/ auttoiko peli ymmärtämään: " + auttoiko);
				Debug.Log("Tietokanta täytetty");
				i++;
				}
		}

	//Update is called once per frame
	void Update () 
		{
		//tarkkailee ollaanko päästy haluttuun blockiin
		Block tietokantaan = Lataa.FindBlock("tietokantaan");
			if(tietokantaan.IsExecuting())
			{
			OnEnter();
			}
		}
    IEnumerator PostScores(int lpy, int asiakast, int tyyt, int potilaat, int virheet, string tulos, string osasto, string kokemus, string auttoiko, string ehdotus)
    {   //Yhdistetään haluttuun .php scriptiin ja lähetetään pisteet mysql stringinä
 
        string post_url = addScoreURL + "&lpy=" + lpy + "&asiakast=" + asiakast + "&tyyt=" + tyyt + "&potilaat=" + potilaat + "&virheet=" + virheet + "&tulosalue=" + tulos + "&osasto=" + osasto + "&kokemus=" + kokemus + "&auttoiko=" + auttoiko + "&ehdotus=" + ehdotus;
 
        // Post the URL to the site and create a download object to get the result.
		WWW hs_post = new WWW(post_url);
        yield return hs_post; // Odotetaan kunnes vahvistus on perillä
 
        if (hs_post.error != null)
		{
            Debug.Log("There was an error posting the high score: " + hs_post.error);
        }
    }
}