﻿using UnityEngine;
using System.Collections;
using Fungus;

public class ajastin_em : MonoBehaviour
{
    public Flowchart em1;
    public float ajastin_e;
    public int ajastin_k;
    // Use this for initialization
    void Start()
    {
        Flowchart em1 = GameObject.FindObjectOfType<Flowchart>();
        Block ajastin = GameObject.FindObjectOfType<Block>();
    }

    // Update is called once per frame

    private void Update()
    {
        Block ajastin = em1.FindBlock("ajastin");

        if(ajastin.IsExecuting())
        {
            ajastin_e += Time.deltaTime;
            ajastin_k = 90 - Mathf.RoundToInt(ajastin_e);
            GameObject.FindObjectOfType<Flowchart>().SetIntegerVariable("ajastin_k", ajastin_k);
        }

    }
}